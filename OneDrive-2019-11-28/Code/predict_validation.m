% close all
% clear
% clc
load('trained2DKernel.mat');
% load('trained2DGauss.mat');
load('multiclass2D_validation.mat');
% load('trained_mnistfashionKernel.mat');
% load('mnistfashion_validation.mat');

Val2D = multiclass2D_validation;
% ValFash = mnistfashion_validation;

Label_Validation_Kernel_2D = trained2DKernel.predictFcn(Val2D); 
% Label_Validation_Gauss_2D = trained2DGauss.predictFcn(Val2D);
% Label_mnistfashion_Validation_Kernel = trained_mnistfashionKernel.predictFcn(ValFash);

% writematrix(Label_Validation_Kernel_2D,"Klassen_Kernel_2D.csv");
%writematrix(Label_Validation_Gauss_2D,"Klassen_Gauss_2D.csv");
% writematrix(Label_mnistfashion_Validation_Kernel,"Klassen_Kernel_fashion.csv");

ende="...Ende...";
ende
